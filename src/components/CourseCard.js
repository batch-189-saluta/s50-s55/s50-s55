// import { useState, useEffect } from 'react'
import { Card, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function CourseCard({courseProp}) {

	// console.log(props)
	// console.log(typeof props)
	// console.log(props.courseProp.name)
	// console.log(courseProp.name)

	/*
		useState - use the state hook for this component to be able to store its state.
		States are used to keep track of information related to individual components

		syntax:
			const [getter, setter] = useState(initialGetterValue)
	*/

	/*const [count, setCount] = useState(0)
	const [seat, setSeat] = useState(30)



	function enroll () {
		// setCount is asyncronous
		//if (count < 30){
			setCount(count + 1)
			setSeat(seat -1)
		// } else {
		// 	alert ("no more seats")
		// }
		console.log('Enrollees: '+count)
	}*/

	// pagnagbago lang yung value ng seat tsaka lang magtatake effect yung function
	/*useEffect(() => {
		if (seat === 0) {
			alert('No more seats available')
		}
	}, [seat])*/
	/*
		useEffect(()=>{},[]) - empty array means isang beses lang siya mag rurun

		useEffect(()=>{}) - no array naman mag rurun siya everytime may changes
	*/



	// Deconstruct the course properties into their own variables
	const {name, description, price, _id} = courseProp;
	console.log(courseProp)

	return (
			<Card>
		      <Card.Body>
		        <Card.Title>{name}</Card.Title>
		    {/*pwede din gamitin card.Title, card.Subtitle, card.Text*/}
		    	<Card.Subtitle>Description:</Card.Subtitle>
		        <Card.Text>{description}</Card.Text>
		        <Card.Subtitle>Price:</Card.Subtitle>
		        <Card.Text>Php {price}</Card.Text>
		        <Button variant="primary" as={Link} to={`/courses/${_id}`}>Details</Button>
		      </Card.Body>
		    </Card>
			
		)
}

