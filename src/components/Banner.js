import {Row, Col, Button} from 'react-bootstrap'

export default function Banner () {

	return (

			<Row>
				{/*className is a property*/}
				<Col className="p-5">
					<h1>Zuitt Coding Bootcamp</h1>
					<p>Opportunitites for everyone, everywhere</p>
					<Button variant="primary">Enroll Now!</Button>
				</Col>
			</Row>
		)
}