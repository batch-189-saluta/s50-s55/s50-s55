import {Link} from 'react-router-dom'

export default function Error () {

	return(
		<>
			<h1>Page Not Found</h1>
			<span>Go back to the </span>
			<Link to="/">HomePage</Link>
		</>
		)
}