import { useState, useEffect, useContext} from 'react'
import { Form, Button } from 'react-bootstrap'
// navigate - redirect
import {Navigate} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Login() {

	// Allows us to consume the User context object and its properties to use for our user validation
	const {user, setUser} = useContext(UserContext)

	// State hook to store the values of the input fields
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	// to determine wether submit button is enabled or not
	const [isActive, setIsActive] = useState(false)


	// useEffect
	useEffect (() => {

		if(email !== "" && password !== "") {

			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[email, password])


	// login function
	function loginUser (e) {

		e.preventDefault()

		/*
			Syntax:
				fetch('url', {options})
				.then(res => res.json())
				.then(data => {})
		*/

		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				// manggagaling sa input fields
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)
			
			if(typeof data.access !== "undefined") {

				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				// pag nagsave na, dapat may lumabas na alert
				Swal.fire({
					title: "Log in Successful",
					icon: "success",
					text: "Welcome to Zuitt"
				})
			} else {

				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login details and try again"
				})
			}
		})




		// Set the email of the authenticated user in the local storage
		/*
			Syntax:
				localStorage.setItem("propertyName", value)
		*/
		// parang nagsesave siya ng item
		// localStorage.setItem("email", email)

		// set the global user state to have properties obtained from local storage
		/*setUser({
			email: localStorage.getItem('email')
		})*/

		// Clear input fields after submission
		setEmail("")
		setPassword("")

		// alert("You are now logged in")
	}

	const retrieveUserDetails = (token) => {

		fetch('http://localhost:4000/users/details', {

			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	return(

		(user.id !== null) ?
			<Navigate to="/courses" />
		:


			<Form className="mt-3" onSubmit={(e) => loginUser(e)}>
				<h1 className="text-center">Log in</h1>
		      <Form.Group className="mb-3" controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
		        	type="email" 
		        	placeholder="Enter email"
		        	value={email}
		        	onChange={e => {
		        		setEmail(e.target.value)
		        	}}
		        	required />
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password1">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		        	type="password" 
		        	placeholder="Password"
		        	value={password}
		        	onChange={e => {setPassword(e.target.value)}}
		        	required />
		      </Form.Group>

		     {
		      	isActive ?
		      		<Button variant="primary" type="submit" id="submitBtn">
				        Submit
				      </Button>
		      :
		      		<Button variant="danger" type="submit" id="submitBtn" disabled>
				        Submit
				      </Button>
		      }
		    </Form>

		)
}